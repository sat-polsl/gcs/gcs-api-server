# Table of Contents

- [Deployment Instructions](docs/DEPLOYMENT.md)
- [Configuration Instructions](docs/CONFIGURATION.md)

---

| _Version_ | `{{ .Version }}` |
|-----------|------------------|
| _Date_    | `{{ .Date }}`    |

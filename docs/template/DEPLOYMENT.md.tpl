# TOC

## Deploying using Docker Compose

```yaml
{{ .DockerCompose }}
```

## OpenTelemetry Collector configuration

```yaml
{{ .OtelCollectorConfig }}
```

---

| _Version_ | `{{ .Version }}` |
|-----------|------------------|
| _Date_    | `{{ .Date }}`    |

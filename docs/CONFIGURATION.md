# Configuration of the GCS Listener

## Configuration file

The configuration file is in TOML format. Application expects it to be placed either in `$HOME/.gcs.toml` or in the current context.

```toml
# `log_level` defines how verbose should the logging system be.
#
# Environment variable: `LOG_LEVEL`
#
# Possible values:
#   - "trace"
#   - "debug"
#   - "info" - default
#   - "warn"
#   - "error"
#   - "fatal"
#   - "panic"
#   - "disabled"
log_level = "info"

# `log_timestamp_format` defines format of timestamp attached to every message.
#
# Environment variable: `LOG_TIMESTAMP_FORMAT`
#
# Possible values:
#   - "UNIX" - default
#   - "UNIXMICRO"
#   - "UNIXNANO"
#   - "UNIXMS"
#   - "RFC3339"
#   - "RFC3339Nano"
#   - "RFC1123"
#   - "RFC1123Z"
#   - "RFC822"
#   - "RFC822Z"
#   - "RFC850"
log_timestamp_format = "UNIX"

# `[listener]` - section describing configuration of listener connection
[listener]

# `host` sets the host on which the listener will be listening
#
# Environment variable: `LISTENER_HOST`
host = "listener"

# `port` sets the port on which the listener will be listening
#
# Environment variable: `LISTENER_PORT`
port = 9000

# `[notification]` - section describing configuration of notifications
[notification]

# `url` sets the URL of the notifications service. It accepts URLs configured
# in the format accepted by the containrrr/shoutrrr library.
#
# If the URLs are specified in the environment variable, they should be separated by a space.
# For example: `NOTIFICATION_URL="discord://token@channel slack://token@channel"`
#
#
# Environment variable: `NOTIFICATION_URLS`
urls = [
    "discord://token@channel",
    "slack://name:token@channel",
]

# `[storage]` - section describing configuration of storage backend
[storage]

# `use` specifies which storage backend should be used
#
# Environment variable: `STORAGE_USE`
#
# Possible values:
#   - "file" - default
#   - "postgres"
use = "file"

    # `[storage.file]` - section describing configuration of file backend
    [storage.file]

    # `output_dir` - specifies the output directory where the data obtained from Proxy should be saved
    #
    # Environment variable: `STORAGE_FILE_OUTPUT_DIR`
    output_dir = "/path/to/storage/destination"

    # `format` defines format of the file
    #
    # Environment variable: `STORAGE_FILE_FORMAT`
    #
    # Possible values:
    #   - "json" - default
    #   - "txt"
    #   - "csv"
    format = "json"

    # `shard_size`
    #
    # Environment variable: `STORAGE_FILE_SHARD_SIZE`
    shard_size = "4MiB"

    # `dump_to_stdout`
    #
    # Environment variable: `STORAGE_FILE_DUMP_TO_STDOUT`
    dump_to_stdout = true

    # `[storage.postgres]` - section describing configuration of PostgreSQL backend
    [storage.postgres]

    # `host` is the host on which the PostgreSQL database is listening.
    #
    # Environment variable: `STORAGE_POSTGRES_HOST`
    host = "postgres"

    # `port` is the port on which the PostgreSQL database is listening
    #
    # Environment variable: `STORAGE_POSTGRES_PORT`
    port = 5432

    # `database` is the name of the PostgreSQL database
    #
    # Environment variable: `STORAGE_POSTGRES_DATABASE`
    database = "database"

    # `username` is the username for the PostgreSQL database
    #
    # Environment variable: `STORAGE_POSTGRES_USERNAME`
    username = "user"

    # `password` is the password for the user specified in `username` field for the PostgreSQL database
    #
    # Environment variable: `STORAGE_POSTGRES_PASSWORD`
    password = "Passw0rd+"

# `[telemetry]` - section describing configuration of telemetry
[telemetry]

# `enabled` sets whether telemetry should be enabled or not
#
# Environment variable: `TELEMETRY_ENABLED`
#
# Default value: `true`
enabled = true

# `otel_host` sets the name of the OpenTelemetry Collector service that will be used to send telemetry data. 
#
# Environment variable: `TELEMETRY_OTEL_SERVICE_NAME`
#
# Default value: `"otel-collector"`
otel_host = "otel-collector"

# `otel_port` sets the port of the OpenTelemetry Collector service that will be used to send telemetry data.
#
# Environment variable: `TELEMETRY_OTEL_SERVICE_PORT`
#
# Default value: `"4317"`
otel_port = "4317"

# `[http]` - section describing configuration of HTTP server
[http]

# `port` sets the port on which the HTTP server will be listening
#
# Environment variable: `HTTP_PORT`
#
# Default value: `8080`
port = 8080

# `insecure` sets whether the HTTP server should be listening on HTTP or HTTPS
#
# Environment variable: `HTTP_INSECURE`
#
# Default value: `false`
insecure = true

# `cert_file` sets the path to the certificate file that will be used to serve HTTPS traffic
#
# Environment variable: `HTTP_CERT_FILE`
cert_file = "/path/to/cert/file"

# `key_file` sets the path to the key file that will be used to serve HTTPS traffic
#
# Environment variable: `HTTP_KEY_FILE`
key_file = "/path/to/key/file"

```

---

| _Version_ | `v0.0.8` |
|-----------|------------------|
| _Date_    | `15 May 23 21:14 CEST`    |

/*
Copyright © 2023 Silesian Aerospace Technologies, GCS Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package heartbeat

import (
	"context"
	"fmt"
	"sync"
	"time"

	"github.com/go-zeromq/zmq4"
	"github.com/rs/zerolog/log"
	"gitlab.com/sat-polsl/gcs/gcs-lib-common/go/grpc"
	"gitlab.com/sat-polsl/gcs/gcs-lib-common/go/storage"
)

// Heartbeat is a struct that contains all the necessary information
// to run the heartbeat loop.
// Both Storage and Configurator are required.
type Heartbeat struct {
	Storage      storage.Storage
	Configurator grpc.ConfiguratorClient

	// mutex for missions
	mux      sync.Mutex
	missions []string
}

// Run starts the heartbeat loop. It sends a heartbeat every second to the
// configurator. It also starts a continuous lister that listens for changes
// in the missions list. List is updated every 5 seconds.
func (h *Heartbeat) Run(ctx context.Context) {
	ticker := time.NewTicker(1 * time.Second)

	go h.continuousLister(ctx)

	for {
		select {
		case <-ticker.C:
			h.do(ctx)

		case <-ctx.Done():
			return
		}
	}
}

func (h *Heartbeat) listMissions(ctx context.Context) ([]string, error) {
	m, err := h.Storage.Get(ctx, "missions", nil)
	if err != nil {
		return nil, err
	}

	missions := []string{}
	for _, v := range m {
		if v["id"] == nil {
			continue
		}

		id, ok := v["id"].(int32)
		if !ok {
			continue
		}

		missions = append(missions, fmt.Sprintf("spacecraft_%d", id))
	}

	return missions, nil
}

func (h *Heartbeat) continuousLister(ctx context.Context) {
	ticker := time.NewTicker(10 * time.Second)

	do := func() {
		missions, err := h.listMissions(ctx)
		if err != nil {
			log.Error().Caller().Err(err).Msg("failed to list missions")
			return
		}

		h.mux.Lock()
		defer h.mux.Unlock()
		h.missions = missions
	}

	for {
		select {
		case <-ctx.Done():
			return

		case <-ticker.C:
			do()
		}
	}
}

func (h *Heartbeat) do(ctx context.Context) {
	log.Trace().Caller().Msg("sending heartbeat")

	h.mux.Lock()
	defer h.mux.Unlock()
	for _, mission := range h.missions {
		_, err := h.Configurator.Set(ctx, &grpc.SetRequest{
			Option: &grpc.Option{
				Name:  zmq4.OptionSubscribe,
				Value: mission,
			},
		})
		if err != nil {
			log.Error().Caller().Str("mission", mission).Err(err).Msg("failed to send heartbeat")
		}
	}
}

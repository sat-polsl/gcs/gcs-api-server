/*
Copyright © 2023 Silesian Aerospace Technologies, GCS Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package cmd

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/containrrr/shoutrrr"
	"github.com/go-chi/chi/v5"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"gitlab.com/sat-polsl/gcs/gcs-api-server/pkg/heartbeat"
	apiv1 "gitlab.com/sat-polsl/gcs/gcs-lib-common/go/api/v1"
	serverv1 "gitlab.com/sat-polsl/gcs/gcs-lib-common/go/api/v1/server"
	gcsgrpc "gitlab.com/sat-polsl/gcs/gcs-lib-common/go/grpc"
	"gitlab.com/sat-polsl/gcs/gcs-lib-common/go/storage"
	"gitlab.com/sat-polsl/gcs/gcs-lib-common/go/storage/file"
	"gitlab.com/sat-polsl/gcs/gcs-lib-common/go/storage/postgres"
)

func realMain(ctx context.Context) error {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	storageClient, err := createStorageClient(ctx, viper.GetString("storage.use"))
	if err != nil {
		return fmt.Errorf("storage client creation failed: %w", err)
	}
	defer timedClose(storageClient)

	configurator, err := createListenerConfigClient()
	if err != nil {
		return fmt.Errorf("grpc client creation failed: %w", err)
	}

	sender, err := shoutrrr.CreateSender(viper.GetStringSlice("notification.urls")...)
	if err != nil {
		return fmt.Errorf("sender creation failed: %w", err)
	}

	// create default server with chi
	r := chi.NewRouter()

	v1 := apiv1.NewStrictHandler(&serverv1.Server{
		Storage:      storageClient,
		Configurator: configurator,
		Sender:       sender,
	}, []apiv1.StrictMiddlewareFunc{})

	apiv1.HandlerFromMux(v1, r)

	httpSrv := &http.Server{
		Handler:           r,
		Addr:              fmt.Sprintf("0.0.0.0:%d", viper.GetInt("http.port")),
		ReadHeaderTimeout: time.Second,
	}

	go func() {
		<-ctx.Done()
		log.Info().Msg("shutting down http server")
		if err := httpSrv.Shutdown(context.Background()); err != nil {
			log.Error().Err(err).Msg("failed to shutdown http server")
		}
	}()

	hb := &heartbeat.Heartbeat{
		Storage:      storageClient,
		Configurator: configurator,
	}

	go hb.Run(ctx)

	log.Info().Msgf("starting http server on %s", httpSrv.Addr)
	if err := httpSrv.ListenAndServe(); err != http.ErrServerClosed {
		return fmt.Errorf("http server failed: %w", err)
	}

	return nil
}

func createStorageClient(ctx context.Context, kind string) (storage.Storage, error) {
	switch kind {
	case "postgres":
		return postgres.New(ctx,
			postgres.WithCredentials(viper.GetString("storage.postgres.username"), viper.GetString("storage.postgres.password")),
			postgres.WithHost(viper.GetString("storage.postgres.host")),
			postgres.WithPort(viper.GetString("storage.postgres.port")),
			postgres.WithDatabase(viper.GetString("storage.postgres.database")),
		)
	case "file":
		return file.NewClient(ctx,
			file.WithFileType(viper.GetString("storage.file.format")),
			file.WithShardSize(viper.GetInt64("storage.file.shard_size")),
			file.WithContentDump(viper.GetBool("storage.file.dump_to_stdout")),
			file.WithStoragePath(viper.GetString("storage.file.output_dir")),
			file.WithTables(map[string][]map[string]interface{}{"missions": {}}),
		), nil
	default:
		return nil, fmt.Errorf("unknown storage type: %s", kind)
	}
}

func createListenerConfigClient() (gcsgrpc.ConfiguratorClient, error) {
	addr := fmt.Sprintf("%s:%d", viper.GetString("listener.host"), viper.GetInt("listener.port"))
	conn, err := grpc.Dial(addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, fmt.Errorf("failed to connect to configurator: %w", err)
	}

	return gcsgrpc.NewConfiguratorClient(conn), nil
}

type closer interface {
	Close(context.Context) error
}

func timedClose(s closer) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	err := s.Close(ctx)
	if err != nil {
		log.Error().Err(err).Msg("failed to close storage client")
	}
	log.Trace().Caller().Msg("storage client closed")
}
